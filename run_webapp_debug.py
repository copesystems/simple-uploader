#!/usr/bin/env python3
from simple_uploader.webapp.app import build_application, application_argument_parser, ApplicationConfiguration

application_argument_parser.add_argument("-b", "--bind-host", default="127.0.0.1", help="IP to bind to.")
application_argument_parser.add_argument("-p", "--bind-port", default=8080, type=int, help="Port to listen on")

if __name__ == "__main__":
    args = application_argument_parser.parse_args()
    app = build_application(ApplicationConfiguration.from_application_args(args))
    app.run(host=args.bind_host, port=args.bind_port, debug=True)
