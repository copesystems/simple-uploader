import time
from passlib.apps import custom_app_context as passlib_context
from bottle import json_loads, json_dumps

# TODO: Write a safe dumps/loads


class User(object):
    def __init__(self, user_id, username, password_data, creation_time, metadata):
        self.user_id = user_id
        self.username = username
        self.password_data = password_data
        self.creation_time = creation_time
        self.metadata = metadata

    def to_row(self, prefix=""):
        return {
            prefix + "userID": self.user_id,
            prefix + "username": self.username,
            prefix + "passwordData": self.password_data,
            prefix + "userCreationTime": self.creation_time,
            prefix + "userJSONMetadata": json_dumps(self.metadata)
        }

    @classmethod
    def from_row(cls, row, prefix=""):
        return cls(
            row["userID"],
            row["username"],
            row["passwordData"],
            row["userCreationTime"],
            json_loads(row["userJSONMetadata"])
        )

    @staticmethod
    def hash_password(password):
        return passlib_context.hash(password)

    def verify_password(self, password):
        if self.password_data:
            return passlib_context.verify(password, self.password_data)
        else:
            return True

    @classmethod
    def new_user(cls, username, password=None, creation_time=None, metadata=None):
        if password is not None:
            password_data = cls.hash_password(password)
        else:
            password_data = None
        return cls(
            None,
            username,
            password_data,
            creation_time or time.time(),
            metadata or {}
        )


class Upload(object):
    def __init__(self, upload_id, name, creation_time, user_id,
                 description, metadata):
        self.upload_id = upload_id
        self.name = name
        self.creation_time = creation_time
        self.user_id = user_id
        self.description = description
        self.metadata = metadata

    def to_row(self, prefix=""):
        return {
            prefix + "uploadID": self.upload_id,
            prefix + "uploadName": self.name,
            prefix + "uploadCreationTime": self.creation_time,
            prefix + "uploadUserID": self.user_id,
            prefix + "uploadDescription": self.description,
            prefix + "uploadJSONMetadata": json_dumps(self.metadata)
        }

    @classmethod
    def from_row(cls, row, prefix=""):
        return cls(
            row[prefix + "uploadID"],
            row[prefix + "uploadName"],
            row[prefix + "uploadCreationTime"],
            row[prefix + "uploadUserID"],
            row[prefix + "uploadDescription"],
            json_loads(row[prefix + "uploadJSONMetadata"])
        )

    # TODO: Write copy method

    @classmethod
    def new_upload(cls, name, creation_time=None, user_id=None, description=None,
                   metadata=None):
        return cls(
            None,
            name,
            creation_time or time.time(),
            user_id,
            description,
            metadata or {}
        )


class UploadVersion(object):
    def __init__(self, row_id, version_id, parent_upload_id, path_on_disk,
                 creation_time, user_id, description, metadata):
        self.row_id = row_id
        self.version_id = version_id
        self.parent_upload_id = parent_upload_id
        self.path_on_disk = path_on_disk
        self.creation_time = creation_time
        self.user_id = user_id
        self.description = description
        self.metadata = metadata

    def to_row(self, prefix=""):
        return {
            prefix + "uploadVersionRowID": self.row_id,
            prefix + "uploadVersionID": self.version_id,
            prefix + "parentUploadID": self.parent_upload_id,
            prefix + "uploadVersionPathOnDisk": self.path_on_disk,
            prefix + "uploadVersionCreationTime": self.creation_time,
            prefix + "uploadVersionUserID": self.user_id,
            prefix + "uploadVersionDescription": self.description,
            prefix + "uploadVersionJSONMetadata": json_dumps(self.metadata)
        }

    @classmethod
    def from_row(cls, row, prefix=""):
        return cls(
            row[prefix + "uploadVersionRowID"],
            row[prefix + "uploadVersionID"],
            row[prefix + "parentUploadID"],
            row[prefix + "uploadVersionPathOnDisk"],
            row[prefix + "uploadVersionCreationTime"],
            row[prefix + "uploadVersionUserID"],
            row[prefix + "uploadVersionDescription"],
            json_loads(row[prefix + "uploadVersionJSONMetadata"])
        )

    # TODO: Write copy method

    @classmethod
    def new_upload_version(cls, version_id, parent_upload_id, user_id,
                           path_on_disk=None, creation_time=None,
                           description=None, metadata=None):
        return cls(
            None,
            version_id,
            parent_upload_id,
            path_on_disk,
            creation_time or time.time(),
            user_id,
            description,
            metadata or {}
        )


class UploadVersionReplicant(object):
    pass
