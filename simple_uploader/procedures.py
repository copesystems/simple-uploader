import os


def generate_path(base_path, upload_id, version_id):
    return os.path.join(base_path, "Upload{0}.Version{1}".format(upload_id, version_id))