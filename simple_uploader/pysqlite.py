import os
import threading
from contextlib import contextmanager
from functools import wraps
from sqlite3 import dbapi2 as sqlite


@contextmanager
def transaction_wrapper(connection):
    try:
        connection.execute("BEGIN DEFERRED TRANSACTION")
        yield connection
    except:
        connection.rollback()
        raise
    else:
        connection.commit()


@contextmanager
def cursor_manager(connection):
    cursor = connection.cursor()
    yield cursor
    cursor.close()


def get_table_count(connection):
    with cursor_manager(connection) as cursor:
        cursor.execute("SELECT count(*) as tableCount FROM sqlite_master "
                       "WHERE type = 'table' AND name != 'sqlite_sequence';")
        return cursor.fetchone()['tableCount']


def optimize_db(connection):
    with cursor_manager(connection) as cursor:
        cursor.execute("PRAGMA optimize")


def vacuum_db(connection):
    with cursor_manager(connection) as cursor:
        cursor.execute("VACUUM")


@wraps(sqlite.connect)
def create_connection(*args, **kwargs):
    conn = sqlite.connect(*args, **kwargs)
    conn.row_factory = sqlite.Row
    return conn


def load_raw_file(filename, folder):
    with open(os.path.join(folder, filename), 'r') as f:
        return f.read()


class ConnectionPool(object):
    def __init__(self, database_location, pragmas=None, *args, **kwargs):
        self.database_location = database_location
        self.pragmas = pragmas
        self.args = args
        self.kwargs = kwargs
        self.pool = {}

    def cursor(self):
        conn = self.get_connection_for_current_thread()
        return conn.cursor()

    def execute(self, *args, **kwargs):
        conn = self.get_connection_for_current_thread()
        return conn.execute(*args, **kwargs)

    def executescript(self, *args, **kwargs):
        conn = self.get_connection_for_current_thread()
        return conn.executescript(*args, **kwargs)

    def executemany(self, *args, **kwargs):
        conn = self.get_connection_for_current_thread()
        return conn.executemany(*args, **kwargs)

    def commit(self, *args, **kwargs):
        conn = self.get_connection_for_current_thread()
        return conn.commit(*args, **kwargs)

    def rollback(self, *args, **kwargs):
        conn = self.get_connection_for_current_thread()
        return conn.rollback(*args, **kwargs)

    def get_connection_for_current_thread(self):
        current_thread_ident = threading.current_thread().ident
        if current_thread_ident in self.pool:
            return self.pool[current_thread_ident]
        else:
            new_conn = create_connection(self.database_location, *self.args, **self.kwargs)
            for pragma in self.pragmas:
                new_conn.execute(pragma)
            self.pool[current_thread_ident] = new_conn
            return new_conn

    def cleanup_for_current_thread(self, raise_for_missing=False):
        current_thread_ident = threading.current_thread().ident
        if current_thread_ident in self.pool:
            return self.pool.pop(current_thread_ident).close()
        else:
            if raise_for_missing:
                raise ValueError("Thread {0} has not current connection!".format(current_thread_ident))

    def wrap_connection_cleanup(self, func, wrap_func=True):
        def closure(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            finally:
                self.cleanup_for_current_thread(raise_for_missing=False)
        return wraps(func)(closure) if wrap_func else closure

    def close(self):
        return self.cleanup_for_current_thread()

    def close_all(self):
        for thread_id in self.pool.keys():
            self.pool.pop(thread_id).close()

    @property
    def profiler(self):
        return self.get_connection_for_current_thread().profiler
