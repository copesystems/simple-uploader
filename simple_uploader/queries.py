from simple_uploader import SQL_FOLDER
from simple_uploader.pysqlite import load_raw_file, cursor_manager
from simple_uploader.models import User, Upload, UploadVersion

import json

SCHEMA = load_raw_file("schema.sql", SQL_FOLDER)


def write_schema(connection):
    with cursor_manager(connection) as cursor:
        cursor.executescript(SCHEMA)


def insert_user(transaction, new_user):
    with cursor_manager(transaction) as cursor:
        cursor.execute(
            """
            INSERT INTO user(
              username, passwordData, userCreationTime, userJSONMetadata
            ) VALUES (
              :username, :passwordData, :userCreationTime, :userJSONMetadata
            )
            """,
            new_user.to_row()
        )
        return cursor.lastrowid


def select_all_users(connection, limit=100):
    with cursor_manager(connection) as cursor:
        cursor.execute("SELECT * FROM user LIMIT ?", (limit,))
        return map(User.from_row, cursor.fetchall())


def select_user_by_username(connection, username):
    with cursor_manager(connection) as cursor:
        cursor.execute("SELECT * FROM user WHERE username=?", (username,))
        row = cursor.fetchone()
        if row:
            return User.from_row(row)
        else:
            return None


def insert_upload(transaction, new_upload):
    with cursor_manager(transaction) as cursor:
        cursor.execute(
            """
            INSERT INTO upload(
              uploadName, uploadCreationTime, uploadUserID, 
              uploadDescription, uploadJSONMetadata
            ) VALUES (
              :uploadName, :uploadCreationTime, :uploadUserID, 
              :uploadDescription, :uploadJSONMetadata
            )            
            """,
            new_upload.to_row()
        )
        return cursor.lastrowid


def select_all_uploads(connection, limit=50):
    with cursor_manager(connection) as cursor:
        cursor.execute("""
            SELECT * FROM upload LIMIT ?
          """, (limit,)
        )
        return map(Upload.from_row, cursor.fetchall())


def select_upload_by_upload_id(connection, upload_id):
    with cursor_manager(connection) as cursor:
        cursor.execute("""
            SELECT * FROM upload WHERE uploadID = ?
          """, (upload_id,)
        )
        row = cursor.fetchone()
        if row:
            return Upload.from_row(row)
        else:
            return None


def insert_upload_version(transaction, new_upload_version):
    with cursor_manager(transaction) as cursor:
        cursor.execute(
            """
            INSERT INTO uploadVersion(
              uploadVersionID, parentUploadID, uploadVersionPathOnDisk,
              uploadVersionCreationTime, uploadVersionUserID,
              uploadVersionDescription, uploadVersionJSONMetadata              
            ) VALUES (
              :uploadVersionID, :parentUploadID, :uploadVersionPathOnDisk,
              :uploadVersionCreationTime, :uploadVersionUserID,
              :uploadVersionDescription, :uploadVersionJSONMetadata
            )
            """,
            new_upload_version.to_row()
        )
        return cursor.lastrowid


def update_upload_version_path_on_disk(transaction, parent_upload_id, upload_version_id, path_on_disk):
    with cursor_manager(transaction) as cursor:
        cursor.execute(
            """
            UPDATE uploadVersion 
            SET uploadVersionPathOnDisk = ? 
            WHERE uploadVersionID = ? AND parentUploadID = ?
            """,
            (path_on_disk, upload_version_id, parent_upload_id)
        )


def select_all_upload_versions(connection, limit=100):
    with cursor_manager(connection) as cursor:
        cursor.execute("SELECT * FROM uploadVersion LIMIT ?", (limit,))


def select_upload_versions_by_parent_upload_id(connection, parent_upload_id):
    with cursor_manager(connection) as cursor:
        cursor.execute(
            "SELECT * FROM uploadVersion WHERE parentUploadID = ?",
            (parent_upload_id,)
        )
        return map(UploadVersion.from_row, cursor.fetchall())


def select_upload_version_by_parent_upload_id_and_version_id(connection, parent_upload_id, version_id):
    with cursor_manager(connection) as cursor:
        cursor.execute(
            "SELECT * FROM uploadVersion WHERE parentUploadID = ? AND uploadVersionID = ?",
            (parent_upload_id, version_id)
        )
        row = cursor.fetchone()
        if row:
            return UploadVersion.from_row(row)
        else:
            return None


def select_max_upload_version_id_by_parent_upload_id(connection, parent_upload_id, default=0):
    with cursor_manager(connection) as cursor:
        cursor.execute(
            "SELECT MAX(uploadVersionID) AS maxUploadVersionID "
            "FROM uploadVersion "
            "WHERE parentUploadID = ?",
            (parent_upload_id,)
        )
        return cursor.fetchone()["maxUploadVersionID"] or default


def get_setting_by_name(connection, setting_name, default=None):
    with cursor_manager(connection) as cursor:
        cursor.execute(
            "SELECT settingJSONValue FROM setting WHERE settingName = ?",
            (setting_name,)
        )
        row = cursor.fetchone()
        if row:
            return json.loads(row["settingJSONValue"])
        else:
            return default


def get_all_settings(connection):
    with cursor_manager(connection) as cursor:
        cursor.execute("SELECT * FROM setting")
        return {
            row["settingName"]: json.loads(row["settingJSONValue"])
            for row in cursor.fetchall()
        }


def set_setting(connection, setting_name, value):
    with cursor_manager(connection) as cursor:
        cursor.execute(
            """
            REPLACE INTO setting(settingName, settingJSONValue)
            VALUES(?, ?)
            """,
            (setting_name, json.dumps(value))
        )
