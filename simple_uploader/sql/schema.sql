PRAGMA JOURNAL_MODE = WAL;
PRAGMA PAGE_SIZE = 8192;

CREATE TABLE IF NOT EXISTS user(
    userID INTEGER PRIMARY KEY,
    username TEXT NOT NULL UNIQUE,
    passwordData TEXT,
    userCreationTime REAL NOT NULL,
    userJSONMetadata TEXT NOT NULL DEFAULT '{}'
);
CREATE INDEX IF NOT EXISTS userUsername_idx ON user(username);

CREATE TABLE IF NOT EXISTS upload(
    uploadID INTEGER PRIMARY KEY,
    uploadName TEXT,
    uploadCreationTime REAL NOT NULL,
    uploadUserID INTEGER REFERENCES user(userID),
    uploadDescription TEXT,
    uploadJSONMetadata TEXT NOT NULL DEFAULT '{}'
);

CREATE TABLE IF NOT EXISTS uploadVersion(
    uploadVersionRowID INTEGER PRIMARY KEY,
    uploadVersionID INTEGER,
    parentUploadID INTEGER NOT NULL REFERENCES upload(uploadID),
    uploadVersionPathOnDisk TEXT,
    uploadVersionCreationTime REAL NOT NULL,
    uploadVersionUserID INTEGER REFERENCES user(userID),
    uploadVersionDescription TEXT,
    uploadVersionJSONMetadata TEXT NOT NULL DEFAULT '{}',
    UNIQUE(uploadVersionID, parentUploadID)
);
CREATE INDEX IF NOT EXISTS uploadVersionParentUploadID_idx ON uploadVersion(parentUploadID);

CREATE TABLE IF NOT EXISTS uploadVersionReplicant(
     uploadVersionReplicantID INTEGER PRIMARY KEY,
     parentUploadVersionRowID INTEGER REFERENCES uploadVersion(uploadVersionRowID),
     uploadVersionReplicantJSONMetadata TEXT NOT NULL DEFAULT '{}'
);
CREATE INDEX IF NOT EXISTS uploadVersionReplicantParentUploadVersionRowID_idx ON uploadVersionReplicant(parentUploadVersionRowID);

CREATE TABLE IF NOT EXISTS setting(
    settingName TEXT PRIMARY KEY,
    settingJSONValue TEXT NOT NULL DEFAULT '{}'
);