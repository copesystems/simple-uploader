from bottle import FileUpload, request, HTTPError, json_dumps, json_loads,\
    HTTPResponse, static_file
import os

from simple_uploader.queries import insert_upload, insert_user, select_upload_by_upload_id,\
    select_max_upload_version_id_by_parent_upload_id, insert_upload_version, \
    select_upload_version_by_parent_upload_id_and_version_id
from simple_uploader.models import Upload, UploadVersion
from simple_uploader.pysqlite import transaction_wrapper
from simple_uploader.procedures import generate_path


class SimpleUploaderAPI(object):
    API_SUB_URI = "/api/1.0"

    def __init__(self, database_connection, config):
        self.database_connection = database_connection
        self.config = config

    def attach_to_application(self, app):
        app.route(self.API_SUB_URI + "/files", method=["POST"])(self.create_new_file)
        app.route(self.API_SUB_URI + "/files/<upload_id:int>", method=["POST"])(self.upload_new_file_version)

        app.route(self.API_SUB_URI + "/files/<upload_id:int>", method=["GET"])(self.download_lastest_file_version)
        app.route(self.API_SUB_URI + "/files/<upload_id:int>/latest", method=["GET"])(self.download_lastest_file_version)
        app.route(self.API_SUB_URI + "/files/<upload_id:int>/metadata", method=["GET"])(self.get_file_metadata)
        app.route(
            self.API_SUB_URI + "/files/<upload_id:int>/version/<version_id:int>", method=["GET"]
        )(self.download_file_version)

        app.route(self.API_SUB_URI + "/files/search")(self.search_for_file)
        app.route(self.API_SUB_URI + "/files/list")(self.list_files)

    def create_new_file(self):
        name = request.forms.get('name')
        if name is None:
            return HTTPError(
                400, json_dumps(
                    {"status": "error", "reason": "Missing required parameter 'name'!"}
                )
            )
        description = request.forms.get('description') or ''

        new_upload = Upload.new_upload(
            name, description=description
        )

        with transaction_wrapper(self.database_connection) as transaction:
            new_upload.upload_id = insert_upload(transaction, new_upload)
        return HTTPResponse(json_dumps({"status": "success", "uploadID": new_upload.upload_id}), 200)

    def upload_new_file_version(self, upload_id):
        parent_upload = select_upload_by_upload_id(self.database_connection, upload_id)
        if parent_upload is None:
            return HTTPError(
                400,
                json_dumps({"status": "error", "reason": "No such Upload ID {0}".format(upload_id)})
            )

        upload_file = request.files.get('upload')
        if upload_file is None:
            return HTTPError(
                400,
                json_dumps({"status": "error", "reason": "File not attached!"})
            )
        description = request.forms.get('description')

        with transaction_wrapper(self.database_connection) as transaction:
            max_version = select_max_upload_version_id_by_parent_upload_id(
                transaction, upload_id, default=0
            )
            path = generate_path(self.config.base_storage_location, upload_id, max_version + 1)
            new_version = UploadVersion.new_upload_version(
                max_version + 1, upload_id, None,
                path_on_disk=path, description=description
            )
            upload_version_row_id = insert_upload_version(transaction, new_version)
        upload_file.save(path)
        return HTTPResponse(
            json_dumps({
                "status": "success", "upload_version_id": max_version + 1,
                "upload_version_row_id": upload_version_row_id,
                "upload_id": upload_id
            })
        )

    def download_lastest_file_version(self, upload_id):
        max_version = select_max_upload_version_id_by_parent_upload_id(
            self.database_connection, upload_id, default=None
        )
        if max_version is None:
            return HTTPError(
                400, json_dumps(
                    {"status": "error", "reason": "No version associated with upload ID {0}".format(upload_id)}
                ))
        else:
            return self.download_file_version(upload_id, max_version)

    def download_file_version(self, upload_id, version_id):
        upload = select_upload_by_upload_id(self.database_connection, upload_id)
        if not upload:
            return HTTPError(
                400, json_dumps({"status": "error", "reason": "No such upload ID {0}".format(upload_id)})
            )
        upload_version = select_upload_version_by_parent_upload_id_and_version_id(self.database_connection, upload_id, version_id)
        if not upload_version:
            return HTTPError(
                400,
                json_dumps({"status": "error",
                            "reason": "No such version ID {0} for upload ID {1}".format(version_id, upload_id)})
            )
        if not upload_version.path_on_disk:
            return HTTPError(
                400,
                json_dumps({"status": "error",
                            "reason": "Version not stored locally!"}
                )
            )
        root, filename = os.path.split(upload_version.path_on_disk)
        return static_file(filename, root)

    def get_file_metadata(self, upload_id):
        pass

    def search_for_file(self):
        pass

    def list_files(self):
        pass
