from bottle import Bottle
from argparse import ArgumentParser

from simple_uploader.webapp.views import SimpleUploaderViews
from simple_uploader.webapp.api import SimpleUploaderAPI
from simple_uploader.queries import write_schema
from simple_uploader.pysqlite import ConnectionPool, get_table_count


class ApplicationConfiguration(object):
    DEFAULT_DATABASE_AUTOCREATE = False
    DEFAULT_DATABASE_PRAGMAS = ("PRAGMA FOREIGN_KEYS = ON",)
    DEFAULT_DATABASE_TIMEOUT = 60
    DEFAULT_TRUST_HTTP_BASIC_AUTH = False

    def __init__(self, database_location, base_storage_location,
                 autocreate_database=DEFAULT_DATABASE_AUTOCREATE,
                 database_pragmas=DEFAULT_DATABASE_PRAGMAS,
                 database_timeout=DEFAULT_DATABASE_TIMEOUT,
                 trust_http_basic_auth=DEFAULT_TRUST_HTTP_BASIC_AUTH):
        self.database_location = database_location
        self.base_storage_location = base_storage_location
        self.autocreate_database = autocreate_database
        self.database_pragmas = database_pragmas
        self.database_timeout = database_timeout
        self.trust_http_basic_auth = trust_http_basic_auth

    @classmethod
    def from_application_args(cls, args):
        return cls(
            args.database_location,
            args.base_storage_location,
            autocreate_database=args.autocreate_database,
            database_timeout=args.database_timeout,
            trust_http_basic_auth=args.trust_http_basic_auth
        )


application_argument_parser = ArgumentParser()
application_argument_parser.add_argument("database_location", type=str, help="SQLite Database location.")
application_argument_parser.add_argument("base_storage_location", type=str, help="Location to put uploads")
application_argument_parser.add_argument(
    "--autocreate-database", action="store_true",
    help="Automatically create the SQLite database if it does not already exist."
)
application_argument_parser.add_argument(
    "-t", "--database-timeout", type=int, default=ApplicationConfiguration.DEFAULT_DATABASE_TIMEOUT,
    help="The database operation timeout parameter."
)
application_argument_parser.add_argument(
    "--trust-http-basic-auth", action="store_true",
    help="Assume the HTTP basic auth being fed in has already been authenticated. "
         "This is useful if the application is being proxied, and the proxy HTTP "
         "server has already performed authentication."
)


def build_application(config):
    app = Bottle()

    database_connection = ConnectionPool(
        config.database_location, pragmas=config.database_pragmas,
        timeout=config.database_timeout
    )
    table_count = get_table_count(database_connection)
    if table_count == 0 and config.autocreate_database:
        write_schema(database_connection)
    elif table_count == 0:
        raise Exception("Database does not exist!")
    api = SimpleUploaderAPI(database_connection, config)
    api.attach_to_application(app)
    views = SimpleUploaderViews(database_connection)
    views.attach_to_application(app)
    return app
