from bottle import SimpleTemplate
import textwrap


view_templates = {
    "base": """
    <html>
        <head>
            <title>
                {{ page_title }}
            </title>
        </head>
        <body>
            {{! content }}
        </body>
    </html>
    """,
    "index": """
    <h1> Simple Uploader </h1>
    """
}


def render_template(name, *args, **kwargs):
    return SimpleTemplate(textwrap.dedent(view_templates[name])).render(*args, **kwargs)


class SimpleUploaderViews(object):
    def __init__(self, database_connection):
        self.database_connection = database_connection

    def attach_to_application(self, app):
        app.route("/", method="GET")(self.index_view)

    def index_view(self):
        index_part = render_template("index")
        return render_template("base", content=index_part, page_title="Simple Uploader")
