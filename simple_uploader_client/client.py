import requests
import shutil
from bottle import json_dumps, json_loads


class Client(object):
    def __init__(self, base_api_url):
        self.base_api_url = base_api_url[:-1] if base_api_url.endswith("/") else base_api_url

    def create_new_upload(self, name, description=None):
        response = requests.post(
            self.base_api_url + "/files",
            data={"name": name, "description": description}
        )
        response.raise_for_status()
        return json_loads(response.content)

    def upload_new_version(self, local_file, upload_id, description=None):
        with open(local_file, 'r') as f:
            response = requests.post(
                self.base_api_url + "/files/{0}".format(upload_id),
                files={"upload": f, "description": description}
            )
            response.raise_for_status()
            return json_loads(response.content)

    def download_latest_version_to_disk(self, local_file_to_write, upload_id):
        with open(local_file_to_write, 'w') as f:
            response = requests.get(self.base_api_url + "/files/{0}/latest".format(upload_id))
            response.raise_for_status()
            for chunk in response.iter_content(8192):
                f.write(chunk)

    def download_version_to_disk(self, local_file_to_write, upload_id, version_id):
        with open(local_file_to_write, 'w') as f:
            response = requests.get(self.base_api_url + "/files/{0}/version/{1}".format(upload_id, version_id))
            response.raise_for_status()
            for chunk in response.iter_content(8192):
                f.write(chunk)
