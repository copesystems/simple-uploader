from unittest import TestCase
from assertpy import assert_that

from simple_uploader.pysqlite import get_table_count, create_connection,\
    transaction_wrapper
from simple_uploader.queries import write_schema, insert_upload, select_all_uploads, insert_user, select_all_users, \
    select_user_by_username, insert_upload_version, select_all_upload_versions, \
    select_upload_versions_by_parent_upload_id, update_upload_version_path_on_disk
from simple_uploader.models import Upload, User, UploadVersion
import time


class QueriesUnitTests(TestCase):
    def setUp(self):
        self.connection = create_connection(":memory:")
        self.connection.execute("PRAGMA FOREIGN_KEYS=ON")
        write_schema(self.connection)

    def tearDown(self):
        self.connection.close()

    def test_write_schema_basic(self):
        assert_that(get_table_count(self.connection)).is_greater_than_or_equal_to(1)

    def test_basic_read_write_users(self):
        user1 = User.new_user('bot1', password='skynet', creation_time=1.0, metadata={'foo': 'bar'})
        user2 = User.new_user('human1', password='resist')

        with transaction_wrapper(self.connection) as transaction:
            insert_user(transaction, user1)
            insert_user(transaction, user2)

        read_back_user = select_user_by_username(self.connection, 'bot1')
        assert_that(read_back_user.username).is_equal_to('bot1')
        assert_that(read_back_user.creation_time).is_equal_to(1.0)
        assert_that(read_back_user.metadata).is_equal_to({'foo': 'bar'})
        assert_that(read_back_user.verify_password('skynet')).is_true()
        assert_that(read_back_user.verify_password('skynot')).is_false()

        read_back_user = select_user_by_username(self.connection, 'human1')
        assert_that(read_back_user.username).is_equal_to('human1')
        assert_that(read_back_user.creation_time).is_between(time.time() - 60, time.time() + 60)
        assert_that(read_back_user.verify_password('resist')).is_true()
        assert_that(read_back_user.verify_password('skynot')).is_false()

        all_users = select_all_users(self.connection)
        all_usernames = [user.username for user in all_users]
        assert_that(all_usernames).contains('bot1', 'human1')

    def test_basic_read_write_uploads(self):
        upload1 = Upload.new_upload("test", description="foobar")
        upload2 = Upload.new_upload("testes")

        with transaction_wrapper(self.connection) as transaction:
            insert_upload(transaction, upload1)
            insert_upload(transaction, upload2)

        uploads = select_all_uploads(self.connection)
        upload_names = [upload.name for upload in uploads]

        assert_that(upload_names).contains("test", "testes")

    def test_basic_read_write_upload_versions(self):

        upload1 = Upload.new_upload("test1")

        with transaction_wrapper(self.connection) as transaction:
            upload1.upload_id = insert_upload(transaction, upload1)

        upload_version_1 = UploadVersion.new_upload_version(1, upload1.upload_id,
                                                            None, path_on_disk="/home/home_user/file",
                                                            creation_time=20, metadata={'stuff': 'no'},
                                                            description="foobarbaz")

        with transaction_wrapper(self.connection) as transaction:
            insert_upload_version(transaction, upload_version_1)

        versions = select_upload_versions_by_parent_upload_id(self.connection, upload1.upload_id)

        versions = list(versions)

        assert_that(versions).is_length(1)
        version = versions[0]
        assert_that(version.version_id).is_equal_to(1)
        assert_that(version.parent_upload_id).is_equal_to(upload1.upload_id)
        assert_that(version.user_id).is_equal_to(None)
        assert_that(version.path_on_disk).is_equal_to("/home/home_user/file")
        assert_that(version.creation_time).is_equal_to(20)
        assert_that(version.metadata).is_equal_to({'stuff': 'no'})
        assert_that(version.description).is_equal_to("foobarbaz")

    def test_upload_version_path_update(self):
        upload1 = Upload.new_upload("test1")

        with transaction_wrapper(self.connection) as transaction:
            upload1.upload_id = insert_upload(transaction, upload1)

        upload_version_1 = UploadVersion.new_upload_version(1, upload1.upload_id,
                                                            None, path_on_disk=None,
                                                            creation_time=20, metadata={'stuff': 'no'},
                                                            description="foobarbaz")

        with transaction_wrapper(self.connection) as transaction:
            version_row_id = insert_upload_version(transaction, upload_version_1)

        versions = select_upload_versions_by_parent_upload_id(self.connection, upload1.upload_id)
        versions = list(versions)
        assert_that(versions).is_length(1)
        version = versions[0]
        assert_that(version.row_id).is_equal_to(version_row_id)
        assert_that(version.path_on_disk).is_none()

        with transaction_wrapper(self.connection) as transaction:
            update_upload_version_path_on_disk(transaction, upload1.upload_id, 1, "/usr/foobar/baz")

        versions = select_upload_versions_by_parent_upload_id(self.connection, upload1.upload_id)
        versions = list(versions)
        assert_that(versions).is_length(1)
        version = versions[0]
        assert_that(version.row_id).is_equal_to(version_row_id)
        assert_that(version.path_on_disk).is_equal_to("/usr/foobar/baz")
